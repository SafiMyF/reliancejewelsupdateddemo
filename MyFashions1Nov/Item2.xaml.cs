﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Nakshatra
{
    /// <summary>
    /// Interaction logic for Item2.xaml
    /// </summary>
    public partial class Item2 : UserControl
    {
        public Item2()
        {
            InitializeComponent();
        }

        public event EventHandler myEventCloseItem2;
        private void Close_TouchDown_1(object sender, TouchEventArgs e)
        {
            myEventCloseItem2(this, null);
        }

        private void Cart_TouchDown_1(object sender, TouchEventArgs e)
        {
            myEventCloseItem2(this, null);
        }

        private void Compare_TouchDown_1(object sender, TouchEventArgs e)
        {
            myEventCloseItem2(this, null);
        }
    }
}
