﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyFashions1Nov
{
    /// <summary>
    /// Interaction logic for Close.xaml
    /// </summary>
    public partial class Close : UserControl
    {
        public CreateDBEntity.Models.productTemplateBeans Samp;

        public event EventHandler MyCloseEvent;
        public event EventHandler MyCartEvent;
        public Close()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts Dragging from ListViewItem To ComparePlayArena");
                this.DataContext = Samp;
                Panel.SetZIndex(imgCompare, 9999);
                imgCompare.Stretch = Stretch.Uniform;
                imgCompare.Source = new ImageSourceConverter().ConvertFromString(Samp.ImgLocation) as ImageSource;
                imgCompare.IsManipulationEnabled = true;
                LogHelper.Logger.Info("Dragged from ListViewItem To ComparePlayArena");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: ClosePage : UserControl_Loaded: " + ex.Message, ex);
                myfashionsecurity.AuthenticationCodeLogic.sendLogEMail("C:/myfashions/Logs/myfashionApp_" + DateTime.Now.ToString("MMddyyyy"));
            }
            
        }
        public void BtnDelete_TouchDown_1(object sender, TouchEventArgs e)
        {
            MyCloseEvent(this, null);
        }
        public void BtnCart_TouchDown_1(object sender, TouchEventArgs e)
        {
            MyCartEvent(this, null);
        }
    }
}
