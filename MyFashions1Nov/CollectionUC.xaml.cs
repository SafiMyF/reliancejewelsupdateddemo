﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Nakshatra
{
    /// <summary>
    /// Interaction logic for CollectionUC.xaml
    /// </summary>
    public partial class CollectionUC : UserControl
    {
        public CollectionUC()
        {
            InitializeComponent();            
        }

        public event EventHandler myEventCollectionLoad;
        Storyboard MyStoryboard;
        private void BtnItem_TouchDown_1(object sender, TouchEventArgs e)
        {
            RenderTransform = new MatrixTransform(0.8, 0, 0, 0.8, 10, 10);
            MyStoryboard = (Storyboard)this.FindResource("CtgListingSB") as Storyboard;
            MyStoryboard.Begin();

            myEventCollectionLoad(this, null);
        }
    }
}
