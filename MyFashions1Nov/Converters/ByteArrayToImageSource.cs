﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace MyFashions1Nov.Converters
{
    //[ValueConversion(typeof(object), typeof(ImageSource))]
    public class ByteArrayToImageSource : IValueConverter
    {
        #region Implementation of IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            try
            {
                Bitmap brush = new Bitmap(Environment.CurrentDirectory + @"\StoreImages\" + value.ToString());
                System.Drawing.Image img = brush.GetThumbnailImage(150, 150, null, System.IntPtr.Zero);
                var imgbrush = new BitmapImage();
                imgbrush.BeginInit();

                imgbrush.StreamSource = convertoStream(img);
                imgbrush.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                imgbrush.EndInit();
                var ib = new ImageBrush(imgbrush);
                return imgbrush;
            }
            catch (Exception)
            {
                return null;
            }

            //return ConvertotImg(value);
        }

        private Stream convertoStream(Image img)
        {
            MemoryStream stream = new MemoryStream();
            //string path = "d:\\samplepng.png";
            img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
            return stream;
        }

        private object ConvertotImg(object value)
        {
            var byteArrayImage = value as byte[];

            if (byteArrayImage != null && byteArrayImage.Length > 0)
            {
                try
                {
                    var ms = new MemoryStream(byteArrayImage);

                    var bitmapImg = new BitmapImage();

                    bitmapImg.BeginInit();
                    bitmapImg.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapImg.DecodePixelWidth = 150;
                    bitmapImg.DecodePixelHeight = 200;
                    bitmapImg.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                    bitmapImg.StreamSource = ms;
                    
                    bitmapImg.EndInit();

                    return bitmapImg;
                }
                catch (Exception)
                {
                    return null;
                }
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        #endregion
    }
}
