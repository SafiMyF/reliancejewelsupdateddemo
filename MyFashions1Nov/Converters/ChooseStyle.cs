﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace MyFashions1Nov.Converters
{
    public class ChooseStyle:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int count = (int)value;
            if (count > 0)
            {
                Style style = Application.Current.FindResource("RectBtnStyle") as Style;
                return style;
            }
            else
            {
                Style style = Application.Current.FindResource("RectBtnStyleDisable") as Style;
                return style;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
