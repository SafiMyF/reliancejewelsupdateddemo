﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Nakshatra
{
    /// <summary>
    /// Interaction logic for CollItemView.xaml
    /// </summary>
    public partial class CollItemView : UserControl
    {
        public CollItemView()
        {
            InitializeComponent();
        }

        public Button CollBtn;
        public Image imgTemp;
        public Image imgTemp1;

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            if (CollBtn != null)
            {
                grdImg.Children.Clear();
                imgTemp = new Image();
                imgTemp1 = CollBtn.Content as Image;
                imgTemp.Source = imgTemp1.Source;
                grdImg.Children.Add(imgTemp);
            }

        }

        public event EventHandler myEventCloseCollIv;
        private void Close_TouchDown_1(object sender, TouchEventArgs e)
        {
            myEventCloseCollIv(this, null);
        }

        private void Cart_TouchDown_1(object sender, TouchEventArgs e)
        {
            myEventCloseCollIv(this, null);
        }

        private void Compare_TouchDown_1(object sender, TouchEventArgs e)
        {
            myEventCloseCollIv(this, null);
        }
    }
}
