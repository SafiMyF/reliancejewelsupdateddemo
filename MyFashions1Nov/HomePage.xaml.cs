﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CreateDBEntity.DBFolder;
using CreateDBEntity.Models;
using System.Configuration;
using System.Reflection;
using System.Windows.Threading;
using System.ComponentModel;
using ImportData.ImportLogic;
using Nakshatra;

namespace MyFashions1Nov
{
    //// <summary>
    //// Interaction logic for HomePage.xaml
    //// </summary>
    public partial class HomePage : Window
    {
        public MyFashionsDBContext DbContext;
        int zIndexVal;
        Configuration config;
        public HomePage()
        {
            InitializeComponent();
            zIndexVal = 9999;
        }

        public CategoryUC Ctg;
        public Storyboard MyStoryboard;
        ItemView Iv =new ItemView();
       
        private void Ctg_myEventStackPanel(object sender, EventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Fetching Data Started from  DbContext to CategoryLeftPanel");
                //CategoryBtnRightPanel.DataContext = DbContext.MainCategory.AsNoTracking().ToList();

                LeftPanel.Visibility = Visibility.Visible;
                stkOfferCollecPanel.Visibility = Visibility.Visible;

                List<CreateDBEntity.Models.MainCategory> lstMainCateg = new List<MainCategory>();
                foreach (var item in DbContext.MainCategory)
                {
                    if (item.Name != "OFFERS" && item.Name != "COLLECTIONS")
                    {
                        lstMainCateg.Add(item);
                    }
                    //List<CreateDBEntity.Models.Category> lst = item.lstCategory.Where(c => c.check == true).ToList();
                    //if (lst.Count > 0)
                    //{
                    //    lstMainCateg.Add(item);
                    //}
                }
                CategoryBtnRightPanel.DataContext = lstMainCateg.OrderBy(c => c.id).ToList();

                LogHelper.Logger.Info("Fetching Data Ended from  DbContext to CategoryLeftPanel");

                CategoryBtnRightPanel.Style = this.FindResource("CategBtnStyle") as Style;

                MyStoryboard = Ctg.FindResource("CtgListingSB") as Storyboard;
                MyStoryboard.Stop();

                Ctg.RenderTransform = new MatrixTransform(0.8, 0, 0, 0.8, 10, 10);
                
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: HomePage : Ctg_myEventStackPanel: " + ex.Message, ex);
                myfashionsecurity.AuthenticationCodeLogic.sendLogEMail("C:/myfashions/Logs/myfashionApp_" + DateTime.Now.ToString("MMddyyyy"));
            }
        }

        public myCustomItemsControl Wl;
        public void Ctg_myEventHand(object sender, EventArgs e)
        {
            try
            {
                Ctg.RenderTransform = new MatrixTransform(0.9, 0, 0, 0.9, 1, 1);
                CreateDBEntity.Models.Category categ = null;
                CreateDBEntity.Models.SubCategory subcateg = null;

                if (sender is Category)
                {
                    categ = sender as CreateDBEntity.Models.Category;
                }
                else
                {
                    subcateg = sender as CreateDBEntity.Models.SubCategory;
                }
                if (Wl == null)
                {
                    Wl = new myCustomItemsControl();
                    Wl.RenderTransform = new MatrixTransform(1, 0, 0, 1, 6, 10);
                }
                if (categ != null)
                {
                    Wl.Categ = categ;
                }
                else if (subcateg != null)
                {
                    Wl.SubCateg = subcateg;
                }
                Wl.dbContext = DbContext;
                Wl.MyItemHandler -= Wl_myItemHandler;
                Wl.MyItemHandler += Wl_myItemHandler;
                Wl.ClearAllItemViews -= Wl_ClearAllItemViews;
                Wl.ClearAllItemViews += Wl_ClearAllItemViews;
                LogHelper.Logger.Info("Checking Children is Available for ContentPanel");
                ContentPanel.DataContext = Wl;

                if (!ContentPanel.Children.Contains(Wl))
                {
                    ContentPanel.Children.Add(Wl);
                    CartComparePanel.Visibility = Visibility.Visible;                    

                    CartComparePanel.Style = this.FindResource("SlideDown") as Style;
                   
                    Wl.RenderTransform = new MatrixTransform(1, 0, 0, 1, 6, 10);
                }
                else
                {
                    Wl.LoadData();
                }
                LogHelper.Logger.Info("Ended Checking Children is Available for ContentPanel");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: HomePage : Ctg_myEventHand: " + ex.Message, ex);
                myfashionsecurity.AuthenticationCodeLogic.sendLogEMail("C:/myfashions/Logs/myfashionApp_" + DateTime.Now.ToString("MMddyyyy"));
            }
            
        }
        void Wl_ClearAllItemViews(object sender, EventArgs e)
        {
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in ContentPanel.Children)
            {
                if (item is ItemView)
                {
                    lstIndexes.Add(ContentPanel.Children.IndexOf(item));
                }
            }

            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();

            foreach (int itemIndex in lstIndexes)
            {
                ContentPanel.Children.RemoveAt(itemIndex);
            }
        }

        int i = 10;

        void Wl_myItemHandler(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            if (btn != null)
            {
                if (i == 200)
                    i = 10;

                var btnLocation = btn.PointToScreen(new Point(btn.ActualHeight, btn.ActualWidth));
                CreateDBEntity.Models.productTemplateBeans samp = btn.DataContext as CreateDBEntity.Models.productTemplateBeans;
                List<CreateDBEntity.Models.productTemplateBeans> lstSamp = new List<CreateDBEntity.Models.productTemplateBeans>();
              
                if (samp.SoldOut == false)
                {
                    lstSamp.Add(samp);
                    try
                    {
                        LogHelper.Logger.Info("Checking  ItemView is Available or Not");
                        if (!CheckChildren(samp))
                        {
                            ItemView iv = new ItemView();

                            iv.MyEventCartCntUpdate -= Wl_myEventCartCntUpdate;
                            iv.MyEventCmpreCntUpdate -= Wl_myEventCmpreCntUpdate;

                            iv.MyEventCartCntUpdate += Wl_myEventCartCntUpdate;
                            iv.MyEventCmpreCntUpdate += Wl_myEventCmpreCntUpdate;
                            iv.ItemViewClose += iv_ItemViewClose;

                            iv.IsManipulationEnabled = true;
                            iv.RenderTransform = new MatrixTransform(1, 0, 0, 1, i+20, i+20);
                            if (samp.category.Name.Contains("JEWELLERY"))
                            {
                                iv.stkJewellery.Visibility = Visibility.Visible;
                                iv.stkApparel.Visibility = Visibility.Collapsed;
                                iv.stkJewellery.DataContext = lstSamp.ToList();
                            }
                            else
                            {
                                iv.stkApparel.Visibility = Visibility.Visible;
                                iv.stkJewellery.Visibility = Visibility.Collapsed;
                                iv.stkPnlBrf.DataContext = lstSamp.ToList();
                            }
                            
                            iv.Samp = lstSamp;
                            ContentPanel.Children.Add(iv);
                            iv.BringIntoView();
                            Panel.SetZIndex(iv, zIndexVal);
                            i = i + 10;
                            
                        }
                        LogHelper.Logger.Info("Ended Checking  ItemView is Available or Not");
                    }
                    catch (Exception ex)
                    {
                        LogHelper.Logger.ErrorException("Exception: HomePage : Wl_myItemHandler: " + ex.Message, ex);
                        myfashionsecurity.AuthenticationCodeLogic.sendLogEMail("C:/myfashions/Logs/myfashionApp_" + DateTime.Now.ToString("MMddyyyy"));
                    }
                }
            }
        }

        void iv_ItemViewClose(object sender, EventArgs e)
        {
            ItemView iView = (ItemView)sender;
            ContentPanel.Children.Remove(iView);
        }

        private bool CheckChildren(CreateDBEntity.Models.productTemplateBeans samp)
        {
            foreach (Control ctrl in ContentPanel.Children)
            {
                if (ctrl is ItemView)
                {
                    ItemView iv = ctrl as ItemView;

                    List<CreateDBEntity.Models.productTemplateBeans> lstSample = iv.stkPnlBrf.DataContext as List<CreateDBEntity.Models.productTemplateBeans>;
                    if (lstSample.Contains(samp))
                    {
                        iv.Visibility = Visibility.Visible;
                        iv.RenderTransform = new MatrixTransform(1, 0, 0, 1, i+20, i+20);
                        iv.BringIntoView();
                        Panel.SetZIndex(iv, zIndexVal++);
                        return true;
                    }
                }
            }
            return false;
        }

        private void Wl_myEventCmpreCntUpdate(object sender, EventArgs e)
        {
            lblCompareCount.Content = StoreCompare_Cart.CmpreCnt;
        }

        private void Wl_myEventCartCntUpdate(object sender, EventArgs e)
        {
            lblCartCount.Content = StoreCompare_Cart.cartCnt;
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            lblLoadingText.Text = "Test Sync";
            InitiateAutoSync();

            
            Storyboard HomeSB = this.TryFindResource("HomeAnimi") as Storyboard;
            HomeSB.Begin();

            HomeAudio.Play();

            #region VideoPlay
            //HomeBgImg.Visibility = Visibility.Hidden;
            //HomeVideo.Visibility = Visibility.Visible;
            //HomeVideo.Play();
            
            #endregion

            #region Manipulation Events
            this.ManipulationStarting -= HomePage_ManipulationStarting;
            this.ManipulationDelta -= HomePage_ManipulationDelta;
            this.ManipulationStarting += HomePage_ManipulationStarting;
            this.ManipulationDelta += HomePage_ManipulationDelta;
            #endregion

            try
            {
                LogHelper.Logger.Info("Executing Connection String..");
                config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);

                //ConnectionStringsSection connectstr = config.ConnectionStrings;
                //ConnectionStringSettings conSettings = connectstr.ConnectionStrings["SqlServerConnectionString"];
                //string str = conSettings.ConnectionString;

                //string[] strArray = str.Split(';');

                //for (int i = 0; i < strArray.Length; i++)
                //{
                //    if (strArray[i].Contains("AttachDbFilename"))
                //    {
                //        string concat = "AttachDbFilename=" + Environment.CurrentDirectory + @"\myFashionDB.mdf";
                //        strArray[i] = concat;
                //    }
                //}

                //string concats = string.Empty;
                //for (int i = 0; i < strArray.Length; i++)
                //{
                //    if (i < strArray.Length - 1)
                //    {
                //        concats += strArray[i] + ";";
                //    }
                //    else
                //    {
                //        concats += strArray[i];
                //    }
                //}

                //conSettings.ConnectionString = concats;
                //conSettings.CurrentConfiguration.Save();
                //config.Save(ConfigurationSaveMode.Modified);
                //ConfigurationManager.RefreshSection("connectionStrings");
                //LogHelper.Logger.Info("Ended Executing Connection String..");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: HomePage : Window Loaded: " + ex.Message, ex);
                myfashionsecurity.AuthenticationCodeLogic.sendLogEMail("C:/myfashions/Logs/myfashionApp_" + DateTime.Now.ToString("MMddyyyy"));
            }
            
        }

        DispatcherTimer timer;
        private void InitiateAutoSync()
        {
            config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            timer = null;
            if (Convert.ToBoolean(config.AppSettings.Settings["AutoSyncEnabled"].Value))
            {
                DateTime fromDate = DateTime.Now;
                DateTime fetchSyncDate = Convert.ToDateTime(DateTime.Today.ToShortDateString() + " " + config.AppSettings.Settings["AutoSyncTiming"].Value);

                TimeSpan tspan = fetchSyncDate - fromDate;

                if (tspan.Seconds < 0)
                {
                    fetchSyncDate = Convert.ToDateTime(DateTime.Today.AddDays(1).ToShortDateString() + " " + config.AppSettings.Settings["AutoSyncTiming"].Value);
                    tspan = fetchSyncDate - fromDate;
                }

                if (timer == null)
                {
                    timer = new DispatcherTimer();
                    timer.Interval = new TimeSpan(Convert.ToInt32(tspan.Hours), Convert.ToInt32(tspan.Minutes), Convert.ToInt32(tspan.Seconds));
                    timer.Tick -= timer_Tick;
                    timer.Tick += timer_Tick;
                    timer.Start();
                }
            }
        }

        BackgroundWorker bgWorker;
        string exceptionMsg = string.Empty;
        void timer_Tick(object sender, EventArgs e)
        {
            config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            syncProcessPanel.Visibility = Visibility.Visible;
            if (bgWorker == null)
            {
                bgWorker = new BackgroundWorker();
                bgWorker.DoWork += bgWorker_DoWork;
                bgWorker.ProgressChanged += bgWorker_ProgressChanged;
                bgWorker.RunWorkerCompleted += bgWorker_RunWorkerCompleted;
                bgWorker.WorkerReportsProgress = true;
                bgWorker.WorkerSupportsCancellation = true;
                bgWorker.RunWorkerAsync();
            }
            timer.Stop();
        }

        void bgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts bgWorker ProgressChanged");
                ProgressNotification progressNotify = e.UserState as ProgressNotification;
                PrgLoadingBar.Minimum = 0;
                PrgLoadingBar.Maximum = progressNotify.ProductsCount;
                PrgLoadingBar.Value = progressNotify.CurrentProduct;
                lblLoadingText.Text = progressNotify.UpdatedMessage;
                LogHelper.Logger.Info("Ends bgWorker ProgressChanged");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: HomePage : bgWorker_ProgressChanged: " + ex.Message, ex);
            }

        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!string.IsNullOrEmpty(exceptionMsg))
                LogHelper.Logger.Info("Auto Sync Exception:" + exceptionMsg);
            syncProcessPanel.Visibility = Visibility.Hidden;
            bgWorker = null;
        }
        void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts bgWorker_DoWork");
                ImportData.AutoSync autosync = new ImportData.AutoSync(config.AppSettings.Settings["SellerID"].Value, config.AppSettings.Settings["SellerEmail"].Value, config.AppSettings.Settings["APIKey"].Value, Utilities.storingPath);
                autosync.progressNotification += autosync_progressNotification;
                autosync.StartSync();
                LogHelper.Logger.Info("Ended bgWorker_DoWork");
            }
            catch (Exception ex)
            {
                exceptionMsg = ex.Message;
                LogHelper.Logger.ErrorException("Exception: HomePage : bgWorker_DoWork: " + ex.Message, ex);
            }
        }
        void autosync_progressNotification(object sender, EventArgs e)
        {
            bgWorker.ReportProgress(1, sender);
        }

        ManipulationModes currentMode = ManipulationModes.All;
        private void HomePage_ManipulationStarting(object sender, ManipulationStartingEventArgs args)
        {
            args.ManipulationContainer = this;
            args.Mode = currentMode;

            // Adjust Z-order
            FrameworkElement element = args.Source as FrameworkElement;
            Panel pnl = element.Parent as Panel;

            for (int i = 0; i < pnl.Children.Count; i++)
            {
                Panel.SetZIndex(pnl.Children[i],
                    pnl.Children[i] == element ? pnl.Children.Count : i);
            }

            // Set Pivot
            Point center = new Point(element.ActualWidth / 2, element.ActualHeight / 2);
            center = element.TranslatePoint(center, this);
            args.Pivot = new ManipulationPivot(center, 48);

            args.Handled = true;
            base.OnManipulationStarting(args);
        }

        private void HomePage_ManipulationDelta(object sender, ManipulationDeltaEventArgs args)
        {
            UIElement element = args.Source as UIElement;
            MatrixTransform xform = element.RenderTransform as MatrixTransform;
            Matrix matrix = xform.Matrix;
            ManipulationDelta delta = args.DeltaManipulation;
            Point center = args.ManipulationOrigin;
            matrix.Translate(-center.X, -center.Y);
            matrix.Scale(delta.Scale.X, delta.Scale.Y);
            matrix.Translate(center.X, center.Y);
            matrix.Translate(delta.Translation.X, delta.Translation.Y);
            xform.Matrix = matrix;
            args.Handled = true;
            base.OnManipulationDelta(args);
        }

        
        Cart crt = new Cart();
        Compare compare = new Compare();

       
        private void BtnTouch_TouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Getting Categories when button touches in homepage");
                Storyboard HomeSB = this.TryFindResource("HomeAnimi") as Storyboard;
                HomeSB.Stop();
                WM_Black.Visibility = Visibility.Collapsed;
                WM_White.Visibility = Visibility.Visible;

                BgImg.Visibility = Visibility.Visible;

                btnAdminSetting.Visibility = Visibility.Hidden;
                PoweredByLogo.Visibility = Visibility.Visible;
                //HomeVideo.Visibility = Visibility.Hidden;
                //HomeVideo.Stop();
                //HomeBgImg.Visibility = Visibility.Visible;

                //GridHeader.Visibility = Visibility.Visible;
                Ctg = new CategoryUC();
                Ctg.IsManipulationEnabled = true;
                Ctg.RenderTransform = new MatrixTransform(0.8, 0, 0, 0.8, 10, 10);

                CreateDBEntity.Models.SuperMainCategory scateg = DbContext.superMainCategory.FirstOrDefault() as CreateDBEntity.Models.SuperMainCategory;
                Ctg.stkPnlMainHeading.DataContext = scateg;
                //Ctg.categoryContext.ItemsSource = scateg.lstMainCategory.OrderBy(c => c.id).ToList();

                List<CreateDBEntity.Models.MainCategory> lstMainCateg = new List<MainCategory>();
                foreach (var item in scateg.lstMainCategory)
                {
                    lstMainCateg.Add(item);
                    //List<CreateDBEntity.Models.Category> lst = item.lstCategory.Where(c => c.check == true).ToList();
                    //if (lst.Count > 0)
                    //{
                    //    lstMainCateg.Add(item);
                    //}
                }
                Ctg.categoryContext.ItemsSource = lstMainCateg.OrderBy(c => c.id).ToList();
                Ctg.txtCount.Text = Ctg.CountIndex(Ctg.categoryContext.Items.Count);

                //Raise events from CategoryUC
                Ctg.MyEventHand += Ctg_myEventHand;
                Ctg.myEventOpenOffers += Ctg_myEventOpenOffers;
                Ctg.myEventOpenCollections += Ctg_myEventOpenCollections;

                ContentPanel.DataContext = Ctg;
                Ctg.MyEventStackPanel += Ctg_myEventStackPanel;
                BtnTouch.Visibility = Visibility.Hidden;
                ContentPanel.Children.Add(Ctg);
                StoreCompare_Cart.cartCnt = 0;
                Wl_myEventCartCntUpdate(this, null);
                StoreCompare_Cart.CmpreCnt = 0;
                Wl_myEventCmpreCntUpdate(this, null);

                compare.canvas1.Children.Clear();
                LogHelper.Logger.Info("Ended Getting Categories when button touches in homepage");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: HomePage :BtnTouchDown: " + ex.Message, ex);
                myfashionsecurity.AuthenticationCodeLogic.sendLogEMail("C:/myfashions/Logs/myfashionApp_" + DateTime.Now.ToString("MMddyyyy"));
            }            
        }
        
        void Ctg_myEventOpenOffers(object sender, EventArgs e)
        {
            LoadOffers();
        }

        void Ctg_myEventOpenCollections(object sender, EventArgs e)
        {
            LoadCollections();
        }

        private void BtnItem_TouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                //string datasourcepath = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + Utilities.storingPath + @"\myFashionDB.mdf;Integrated Security=True;Connect Timeout=30";
                //DbContext = new CreateDBEntity.DBFolder.MyFashionsDBContext(datasourcepath);


                LogHelper.Logger.Info("Starts Fetching Data when btnItem TouchDown");
                Button btn = sender as Button;
                CreateDBEntity.Models.MainCategory data = btn.DataContext as CreateDBEntity.Models.MainCategory;
                List<CreateDBEntity.Models.MainCategory> lstData = new List<MainCategory>();
                lstData.Add(data);

                if (data.lstCategory.Count == 1)
                {
                    Ctg.categoryContext.ItemsSource = data.lstCategory[0].subCategory.Where(c => c.check == true);
                    LeftPanel.Visibility = Visibility.Visible;
                    Ctg.txtCount.Text = Ctg.CountIndex(Ctg.categoryContext.Items.Count);//data.lstCategory[0].subCategory.Count);
                }
                else
                {
                    Ctg.DataContext = data.lstCategory;
                    Ctg.categoryContext.ItemsSource = data.lstCategory.Where(c => c.check == true).OrderBy(c => c.id).ToList();
                    Ctg.txtCount.Text = Ctg.CountIndex(Ctg.categoryContext.Items.Count);//data.lstCategory.Count);
                }

                Ctg.stkPnlMainHeading.DataContext = data;
                Ctg.DataContext = data;
                Ctg.Refresh();
                ContentPanel.Children.Clear();
                ContentPanel.DataContext = Ctg;
                if (!ContentPanel.Children.Contains(Ctg))
                {
                    ContentPanel.Children.Add(Ctg);
                    Ctg.grid.Style = this.FindResource("SlideUpStyle") as Style;
                }
                LogHelper.Logger.Info("Stop Fetching Data when btnItem TouchDown");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: HomePage :BtnItemTouchDown: " + ex.Message, ex);
                myfashionsecurity.AuthenticationCodeLogic.sendLogEMail("C:/myfashions/Logs/myfashionApp_" + DateTime.Now.ToString("MMddyyyy"));
            }            
        }

        private void BtnHome_TouchDown(object sender, TouchEventArgs e)
        {

            zIndexVal = 9999;

            Storyboard HomeSB = this.TryFindResource("HomeAnimi") as Storyboard;
            HomeSB.Seek(TimeSpan.Zero);
            stkOfferCollecPanel.Visibility = Visibility.Collapsed;

            WM_Black.Visibility = Visibility.Visible;
            WM_White.Visibility = Visibility.Collapsed;

            BgImg.Visibility = Visibility.Collapsed;
            btnAdminSetting.Visibility = Visibility.Visible;
            PoweredByLogo.Visibility = Visibility.Hidden;
            //HomeBgImg.Visibility = Visibility.Hidden;
            //HomeAudio.Stop();
            //HomeVideo.Visibility = Visibility.Visible;
            //HomeVideo.Play();

            CartComparePanel.Visibility = Visibility.Hidden;
            //GridHeader.Visibility = Visibility.Hidden;
            ContentPanel.Children.Clear();
            StoreCompare_Cart.lstCompare.Clear();
            StoreCompare_Cart.lstCart.Clear();
            StoreCompare_Cart.lstSelectedCompare.Clear();
            LeftPanel.Visibility = Visibility.Collapsed;
            BtnTouch.Visibility = Visibility.Visible;
            string datasourcepath = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + Utilities.storingPath + @"\myFashionDB.mdf;Integrated Security=True;Connect Timeout=30";
            DbContext = new CreateDBEntity.DBFolder.MyFashionsDBContext(datasourcepath);
        }
      

        private void BtnCart_TouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts when ButtonCartTouchDown Enters");
                crt.MyEventCartCntUpdate += Wl_myEventCartCntUpdate;
                crt.MyEventCmpreCntUpdate += Wl_myEventCmpreCntUpdate;

                ContentPanel.Children.Clear();
                if (!ContentPanel.Children.Contains(crt))
                {
                    ContentPanel.Children.Add(crt);
                }
                LogHelper.Logger.Info("Stops when ButtonCartTouchDown Enters");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: HomePage :BtnCart_TouchDown: " + ex.Message, ex);
                myfashionsecurity.AuthenticationCodeLogic.sendLogEMail("C:/myfashions/Logs/myfashionApp_" + DateTime.Now.ToString("MMddyyyy"));
            }
        }

        private void BtnCompare_TouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts When BtnCompare_TouchDown Enters ");
                compare.MyEventCmpreCntUpdate -= Wl_myEventCmpreCntUpdate;
                compare.MyEventCartCntUpdate -= Wl_myEventCartCntUpdate;
                compare.MyEventCmpreCntUpdate += Wl_myEventCmpreCntUpdate;
                compare.MyEventCartCntUpdate += Wl_myEventCartCntUpdate;
                ContentPanel.Children.Clear();
                if (!ContentPanel.Children.Contains(compare))
                {
                    ContentPanel.Children.Add(compare);
                }
                LogHelper.Logger.Info("Stops When BtnCompare_TouchDown Enters ");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: HomePage :BtnCompare_TouchDown: " + ex.Message, ex);
            }
            
        }

        private void HomeVideo_MediaEnded_1(object sender, RoutedEventArgs e)
        {
            //HomeVideo.Position = new TimeSpan(0,0,1);
            //HomeVideo.Play();
        }

        AdminLogin AdmLogin;
        private void btnAdminSetting_TouchDown_1(object sender, TouchEventArgs e)
        {
            LoadAdminLogin();
        }

        private void LoadAdminLogin()
        {
            btnAdminSetting.Visibility = Visibility.Hidden;
            AdmLogin = new AdminLogin();
            Settings.Children.Clear();
            Settings.Children.Add(AdmLogin);
            AdmLogin.myEventCloseAdminSetting += AdmLogin_myEventCloseAdminSetting;
            AdmLogin.myEventLoginAdmin += AdmLogin_myEventLoginAdmin; 
        }

        private void LoadAdminSettings()
        {
            AdmSetting = new AdminSettings();
            Settings.Children.Clear();
            Settings.Children.Add(AdmSetting);
            AdmSetting.myEventAppDetails += AdmSetting_myEventAppDetails;
            AdmSetting.myEventAppSync += AdmSetting_myEventAppSync;
            AdmSetting.myEventLogoutSettings += AdmSetting_myEventLogoutSettings;
        }

        void AdmSetting_myEventLogoutSettings(object sender, EventArgs e)
        {
            Settings.Children.Clear();
            btnAdminSetting.Visibility = Visibility.Visible;
        }

        void AdmSetting_myEventAppSync(object sender, EventArgs e)
        {
            LoadSync();
        }

        private void LoadSync()
        {
            SetAutoSync setsync = new SetAutoSync();
            Settings.Children.Clear();
            Settings.Children.Add(setsync);
            setsync.myEventBacktoAdminSettings += setsync_myEventBacktoAdminSettings;
            setsync.autoSyncRefresh += setsync_autoSyncRefresh;
        }

        void setsync_autoSyncRefresh(object sender, EventArgs e)
        {
            InitiateAutoSync();
        }

        void setsync_myEventBacktoAdminSettings(object sender, EventArgs e)
        {
            LoadAdminSettings();
        }

        void AdmLogin_myEventLoginAdmin(object sender, EventArgs e)
        {
            LoadAdminSettings();
        }

        AdminSettings AdmSetting;
        void AdmLogin_myEventCloseAdminSetting(object sender, EventArgs e)
        {
            btnAdminSetting.Visibility = Visibility.Visible;
            Settings.Children.Clear();
        }

        AppDetails AppDets;
        void AdmSetting_myEventAppDetails(object sender, EventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts AdmSetting_myEventAppDetails");
                AppDets = new AppDetails();
                Settings.Children.Clear();
                Settings.Children.Add(AppDets);
                AppDets.startManualSync += AppDets_startManualSync;
                AppDets.myEventBacktoAdminSetting += AppDets_myEventBacktoAdminSetting;
                LogHelper.Logger.Info("Ends AdmSetting_myEventAppDetails");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: HomePage :AdmSetting_myEventAppDetails: " + ex.Message, ex);
            }

        }

        void AppDets_startManualSync(object sender, EventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts AppDets_startManualSync");
                config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
                syncProcessPanel.Visibility = Visibility.Visible;
                if (bgWorker == null)
                {
                    syncProcessPanel.Visibility = Visibility.Visible;
                    bgWorker = new BackgroundWorker();
                    bgWorker.DoWork += bgWorker_DoWork;
                    bgWorker.ProgressChanged += bgWorker_ProgressChanged;
                    bgWorker.RunWorkerCompleted += bgWorker_RunWorkerCompleted;
                    bgWorker.WorkerReportsProgress = true;
                    bgWorker.WorkerSupportsCancellation = true;
                    bgWorker.RunWorkerAsync();
                }
                LogHelper.Logger.Info("Ends AppDets_startManualSync");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: HomePage :AppDets_startManualSync: " + ex.Message, ex);
            }

        }

        void AppDets_myEventBacktoAdminSetting(object sender, EventArgs e)
        {
            LoadAdminSettings();
        }

        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
        }

        private void HomeAudio_MediaEnded_1(object sender, RoutedEventArgs e)
        {
            HomeAudio.Position = new TimeSpan(0, 0, 1);
            HomeAudio.Play();
        }

        private void btnOffers_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            LoadOffers();
        }

        private void LoadOffers()
        {
            Offers Offrs = new Offers();
            ContentPanel.Children.Clear();
            ContentPanel.Children.Add(Offrs);

            Offrs.myEventLoadListing += Offrs_myEventLoadListing;
        }

        void Offrs_myEventLoadListing(object sender, EventArgs e)
        {
            ContentPanel.Children.Clear();
            LoadCollectionItemControl();
        }

        private void btnCollections_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            LoadCollections();
        }

        private void LoadCollections()
        {
            CollectionUC Coll = new CollectionUC();
            ContentPanel.Children.Clear();

            Coll.IsManipulationEnabled = true;
            Coll.RenderTransform = new MatrixTransform(0.8, 0, 0, 0.8, 10, 10);
            ContentPanel.Children.Add(Coll);

            Coll.myEventCollectionLoad += Coll_myEventCollectionLoad;
        }

        void Coll_myEventCollectionLoad(object sender, EventArgs e)
        {
            LoadCollectionItemControl();
        }

        int ItemsCount = 0;
        private void LoadCollectionItemControl()
        {
            CollectionItemsControl CIC = new CollectionItemsControl();
            foreach (UserControl item in ContentPanel.Children)
            {
                if (item is CollectionItemsControl)
                {
                    ItemsCount = 1;
                }
            }

            if (ItemsCount == 0)
                ContentPanel.Children.Add(CIC);

            ItemsCount = 0;

            CIC.myItemViewCollection += CIC_myItemViewCollection;
            CIC.myEventOpenItem2 += CIC_myEventOpenItem2;
            CIC.myEventOpenItem3 += CIC_myEventOpenItem3;
        }
        
        void CIC_myEventOpenItem2(object sender, EventArgs e)
        {
            if (i == 200)
                i = 20;

            Item2 cIV2 = new Item2();
            ContentPanel.Children.Add(cIV2);
            cIV2.IsManipulationEnabled = true;
            cIV2.RenderTransform = new MatrixTransform(1, 0, 0, 1, i + 20, i + 20);
            cIV2.BringIntoView();
            Panel.SetZIndex(cIV2, zIndexVal);
            i = i + 10;
            cIV2.myEventCloseItem2 += cIV2_myEventCloseItem2;

        }

        void cIV2_myEventCloseItem2(object sender, EventArgs e)
        {
            Item2 iView = (Item2)sender;
            ContentPanel.Children.Remove(iView);
        }

        void CIC_myEventOpenItem3(object sender, EventArgs e)
        {
            if (i == 200)
                i = 20;

            Item3 cIV3 = new Item3();
            ContentPanel.Children.Add(cIV3);
            cIV3.IsManipulationEnabled = true;
            cIV3.RenderTransform = new MatrixTransform(1, 0, 0, 1, i + 20, i + 20);
            cIV3.BringIntoView();
            Panel.SetZIndex(cIV3, zIndexVal);
            i = i + 10;
            cIV3.myEventCloseItem3 += cIV3_myEventCloseItem3;
        }

        void cIV3_myEventCloseItem3(object sender, EventArgs e)
        {
            Item3 iView = (Item3)sender;
            ContentPanel.Children.Remove(iView);
        }
      
        void CIC_myItemViewCollection(object sender, EventArgs e)
        {
            if (i == 200)
                i = 20;

            CollItemView cIV = new CollItemView();
            ContentPanel.Children.Add(cIV);

            Button btnDets = sender as Button;
            cIV.CollBtn = btnDets;
            cIV.IsManipulationEnabled = true;
            cIV.RenderTransform = new MatrixTransform(1, 0, 0, 1, i + 20, i + 20);
            cIV.BringIntoView();
            Panel.SetZIndex(cIV, zIndexVal);
            i = i + 10;

            cIV.myEventCloseCollIv += cIV_myEventCloseCollIv;            
        }

        void cIV_myEventCloseCollIv(object sender, EventArgs e)
        {
            CollItemView iView = (CollItemView)sender;
            ContentPanel.Children.Remove(iView);
        }
              
    }
}
