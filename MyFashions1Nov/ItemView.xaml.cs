﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CreateDBEntity.DBFolder;
using CreateDBEntity.Models;


namespace MyFashions1Nov
{
    /// <summary>
    /// Interaction logic for ItemView.xaml
    /// </summary>
    public partial class ItemView : UserControl
    {
        
        public ItemView()
        {
            InitializeComponent();
            
        }

        public List<CreateDBEntity.Models.productTemplateBeans> Samp;
        public event EventHandler MyEventCartCntUpdate;
        public event EventHandler MyEventCmpreCntUpdate;
        public event EventHandler ItemViewClose;
        public Button Btn;
        

        private void Close_TouchDown(object sender, TouchEventArgs e)
        {
            ItemViewClose(this, null);
        }

        public Compare cmp;

        private void Compare_TouchDown(object sender, TouchEventArgs e)
        {
            //if (sellerSizes.SelectedItems.Count > 0)
            //{
                try
                {
                    bool issuccess = false;
                    LogHelper.Logger.Info("Starts When ItemView Compare TouchDown Enters");
                    Samp = stkPnlBrf.DataContext as List<CreateDBEntity.Models.productTemplateBeans>;

                    foreach (CreateDBEntity.Models.productTemplateBeans lstSampl in Samp)
                    {
                        List<CreateDBEntity.Models.productTemplateBeans> lstprod = StoreCompare_Cart.lstCompare.Where(c => c.ProdTemplateID.Equals(lstSampl.ProdTemplateID) && c.SNo.Equals(lstSampl.SNo)).ToList();

                        List<CreateDBEntity.Models.productTemplateBeans> lstSelCmp = StoreCompare_Cart.lstSelectedCompare.Where(c => c.ProdTemplateID.Equals(lstSampl.ProdTemplateID) && c.SNo.Equals(lstSampl.SNo)).ToList();

                        if (lstprod.Count == 0 && lstSelCmp.Count == 0)
                        {
                            SellerSizes sellersize = sellerSizes.SelectedItem as SellerSizes;
                            issuccess = true;
                            StoreCompare_Cart.lstCompare.Add(lstSampl);
                            //if (StoreCompare_Cart.lstMaintainSelectedSizes.ContainsKey(lstSampl.ProdTemplateID))
                            //    StoreCompare_Cart.lstMaintainSelectedSizes[lstSampl.ProdTemplateID] = Convert.ToInt32(sellersize.sellerSizeValue);
                            //else
                            //    StoreCompare_Cart.lstMaintainSelectedSizes.Add(lstSampl.ProdTemplateID, Convert.ToInt32(sellersize.sellerSizeValue));
                            StoreCompare_Cart.CmpreCnt += 1;
                            MyEventCmpreCntUpdate(this, null);
                        }
                        else
                        {
                            MessageBox.Show("Selected item is already added to Compare. Please choose another one....", "My Fashions", MessageBoxButton.OK, MessageBoxImage.Information);

                            this.Visibility = Visibility.Hidden;
                        }
                    }

                    if (issuccess)
                    {
                        System.Threading.Thread.Sleep(500);
                        this.Visibility = Visibility.Hidden;
                    }
                    LogHelper.Logger.Info("Stops When ItemView Compare TouchDown Enters");
                }
                catch (Exception ex)
                {
                    LogHelper.Logger.ErrorException("Exception: ItemViewPage : CompareTouchDown: " + ex.Message, ex);
                    myfashionsecurity.AuthenticationCodeLogic.sendLogEMail("C:/myfashions/Logs/myfashionApp_" + DateTime.Now.ToString("MMddyyyy"));
                }
            //}
        }

        private void Cart_TouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                //if (sellerSizes.SelectedItems.Count > 0)
                //{
                    LogHelper.Logger.Info("Starts When Cart TouchDown Enters");
                    bool issuccess = false;
                    Samp = stkPnlBrf.DataContext as List<CreateDBEntity.Models.productTemplateBeans>;
                    foreach (CreateDBEntity.Models.productTemplateBeans sampSngle in Samp)
                    {
                        List<CreateDBEntity.Models.productTemplateBeans> lstprod = StoreCompare_Cart.lstCart.Where(c => c.ProdTemplateID.Equals(sampSngle.ProdTemplateID) && c.SNo.Equals(sampSngle.SNo)).ToList();
                        if (lstprod.Count == 0)
                        {
                            SellerSizes sellersize = sellerSizes.SelectedItem as SellerSizes;
                            issuccess = true;
                            StoreCompare_Cart.lstCart.Insert(0, sampSngle);
                            //if (StoreCompare_Cart.lstMaintainSelectedSizes.ContainsKey(sampSngle.ProdTemplateID))
                            //    StoreCompare_Cart.lstMaintainSelectedSizes[sampSngle.ProdTemplateID] = Convert.ToInt32(sellersize.sellerSizeValue);
                            //else
                            //    StoreCompare_Cart.lstMaintainSelectedSizes.Add(sampSngle.ProdTemplateID, Convert.ToInt32(sellersize.sellerSizeValue));
                            StoreCompare_Cart.cartCnt += 1;
                            MyEventCartCntUpdate(this, null);
                        }
                        else
                        {
                            MessageBox.Show("Already added the selected item to cart. Please choose another one....", "My Fashions", MessageBoxButton.OK, MessageBoxImage.Information);
                            this.Visibility = Visibility.Hidden;
                        }
                    }

                    if (issuccess)
                    {
                        this.Visibility = Visibility.Hidden;
                    }
                    LogHelper.Logger.Info("Stops When Cart TouchDown Enters");
                //}
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: ItemViewPage : CartTouchDown: " + ex.Message, ex);
                myfashionsecurity.AuthenticationCodeLogic.sendLogEMail("C:/myfashions/Logs/myfashionApp_" + DateTime.Now.ToString("MMddyyyy"));
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            stkPnlBrf.DataContext = Samp;
            
            
            stkPricePanel.DataContext = Samp;

            //prodTemp = Samp as List<CreateDBEntity.Models.productTemplateBeans>;
            
            
            if (txtDiscountPercent.Text == "0.00")
            {
                stkDiscountPanel.Visibility = Visibility.Collapsed;
                txtDiscountPrice.Text = Math.Round(Convert.ToDecimal(txtPrice.Text)).ToString();
            }
            else
            {
                Decimal Price = Convert.ToDecimal(txtPrice.Text);
                Decimal DiscountPercent = Convert.ToDecimal(txtDiscountPercent.Text);
                
                stkDiscountPanel.Visibility = Visibility.Visible;

                txtDiscountPrice.Text = Math.Round(Convert.ToDecimal((Price- (Price * (DiscountPercent / 100))))).ToString();
            }
        }

        private void BtnThumbs_TouchDown(object sender, TouchEventArgs e)
        {
            Image img = e.OriginalSource as Image;
            imgMain.Source = img.Source;
        }

        private void sellerSizes_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}



