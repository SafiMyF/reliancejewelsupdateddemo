﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MyFashions1Nov
{
    /// <summary>
    /// Interaction logic for SplashScreen.xaml
    /// </summary>
    public partial class SplashScreen : Window
    {
        public SplashScreen()
        {
            InitializeComponent();
        }

        BackgroundWorker bgw = null;
        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            bgw = new BackgroundWorker();
            bgw.WorkerReportsProgress = true;
            bgw.WorkerSupportsCancellation = true;
            bgw.DoWork += bgw_DoWork;
            bgw.ProgressChanged += bgw_ProgressChanged;
            bgw.RunWorkerCompleted += bgw_RunWorkerCompleted;
            bgw.RunWorkerAsync();
        }

        void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                string appPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                if (myfashionsecurity.AuthenticationCodeLogic.CheckProductHasLicense(appPath, dbContext))
                {
                   System.Threading.Thread.Sleep(1000);
                    HomePage homePage = new HomePage();
                    homePage.DbContext = dbContext;
                    this.Hide();
                    homePage.ShowDialog();
                    this.Close();
                    //MyFashions1Nov.Properties.Settings.Default.SellerEmail = "email here";
                    //MyFashions1Nov.Properties.Settings.Default.Save();
                }
            }
            catch (myfashionsecurity.SecurityException securityexc)
            {
                MessageBox.Show(securityexc.Message.ToString(), "myfashions", MessageBoxButton.OK, MessageBoxImage.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "myfashions", MessageBoxButton.OK, MessageBoxImage.Information);
                LogHelper.Logger.ErrorException(ex.Message, ex);
                myfashionsecurity.AuthenticationCodeLogic.sendLogEMail("C:/myfashions/Logs/myfashionApp_" + DateTime.Now.ToString("MMddyyyy"));
            }
        }

        void bgw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            txtText.Text = e.UserState as string;
        }

        CreateDBEntity.DBFolder.MyFashionsDBContext dbContext;
        void bgw_DoWork(object sender, DoWorkEventArgs e)
        {
            bgw.ReportProgress(1, "Initializing Database . . . Please wait . . .");
            string datasourcepath = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + Utilities.storingPath + @"\myFashionDB.mdf;Integrated Security=True;Connect Timeout=30";
            dbContext = new CreateDBEntity.DBFolder.MyFashionsDBContext(datasourcepath);
            bgw.ReportProgress(1, "Loading Database . . . Please wait . . . ");
            var database = dbContext.Catalog.ToList();
            bgw.ReportProgress(1, "Verifying License Information . . . Please wait . . . ");
        }
    }
}
