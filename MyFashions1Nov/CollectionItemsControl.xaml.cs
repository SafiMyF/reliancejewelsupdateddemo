﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using CreateDBEntity.DBFolder;
using CreateDBEntity.Models;
using System.Windows.Media.Animation;
using System.Linq.Dynamic;

namespace Nakshatra
{
    /// <summary>
    /// Interaction logic for CollectionItemsControl.xaml
    /// </summary>
    public partial class CollectionItemsControl : UserControl
    {
        public CollectionItemsControl()
        {
            InitializeComponent();
        }

        public MyFashionsDBContext dbContext;
        public CreateDBEntity.Models.Category Categ;
        public CreateDBEntity.Models.SubCategory SubCateg;
        public CreateDBEntity.Models.ProductImageBeans ImgBean;
        public CreateDBEntity.Models.ProductStyleBeans PsBean;
        public CreateDBEntity.Models.productTemplateBeans PtBean;
        public CreateDBEntity.Models.Store Store;
        bool isScrollMoved = false;
        Button btnTouchedItem = null;
        public delegate void MyItemView(object sender, EventArgs e);
        //public event MyItemView MyItemHandler;
        public event MyItemView myItemViewCollection;
        public event EventHandler ClearAllItemViews;

        public void LoadData()
        {
            
        }

        private void loadNoStockImg()
        {
        }

        private void BtnItem_TouchDown(object sender, TouchEventArgs e)
        {
            try
            {
               // LogHelper.Logger.Info("When Listing BtnItem TouchDown Starts");
                Button btn = sender as Button;
                btnTouchedItem = btn;
                if (!isScrollMoved)
                {
                    myItemViewCollection(btnTouchedItem, null);
                }
                else
                    isScrollMoved = false;
                //LogHelper.Logger.Info("When Listing BtnItem TouchDown Ends");
            }
            catch (Exception ex)
            {
                //LogHelper.Logger.ErrorException("Exception: myCustomItemsControlPage : BtnItem_TouchDown: " + ex.Message, ex);
            }
        }

        private void ScrollViewer_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        List<productTemplateBeans> lstProdTemplateBeans;
        List<productTemplateBeans> lstMainProdTemplateBeans;

        private void chkStore_Checked_1(object sender, RoutedEventArgs e)
        {
            if (sender is CheckBox)
            {
                //ImageContext.ItemsSource = null;
                CheckBox chkBox = sender as CheckBox;
                CreateDBEntity.Models.Store fetchStore = chkBox.DataContext as Store;

                List<productTemplateBeans> lstProds = fetchStore.prodTemp.Where(c => c.subCategory.id == SubCateg.id).ToList();
                lstMainProdTemplateBeans.AddRange(lstProds);
                //ImageContext.ItemsSource = lstMainProdTemplateBeans;

            }
        }

        private void chkStore_Unchecked_1(object sender, RoutedEventArgs e)
        {
            if (sender is CheckBox)
            {
                //ImageContext.ItemsSource = null;
                CheckBox chkBox = sender as CheckBox;
                CreateDBEntity.Models.Store fetchStore = chkBox.DataContext as Store;
                List<productTemplateBeans> lstProds = fetchStore.prodTemp.Where(c => c.subCategory.id == SubCateg.id).ToList();

                lstProds.ForEach(c =>
                {
                    lstMainProdTemplateBeans.Remove(c);
                });

                if (lstMainProdTemplateBeans.Count == 0)
                    //ImageContext.ItemsSource = null;
                //else
                    //ImageContext.ItemsSource = lstMainProdTemplateBeans;
                loadNoStockImg();
            }

        }

        private void fabricCollection_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            PrepareExpression();
        }
        private void PhotoCollection_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            PrepareExpression();
        }

        private void CboPrice_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            PrepareExpression();
        }

        private void PrepareExpression()
        {
            //ScrollViewerImageContext.ScrollToVerticalOffset(0);
            //string pricequery = string.Empty;
            //string mainExpr = string.Empty;
            //if (PhotoCollection != null)
            //{
            //    bool colorStart = false;
            //    var lstCatalogColors = PhotoCollection.SelectedItems;
            //    for (int i = 0; i < lstCatalogColors.Count; i++)
            //    {
            //        CatalogColors catalogColor = lstCatalogColors[i] as CatalogColors;
            //        if (string.IsNullOrEmpty(mainExpr))
            //        {
            //            if (lstCatalogColors.Count > 0)
            //            {
            //                colorStart = true;
            //                mainExpr += "(ProdColor.id=" + catalogColor.id + " ";
            //            }
            //            else
            //                mainExpr += "ProdColor.id=" + catalogColor.id + " ";
            //        }
            //        else if (!string.IsNullOrEmpty(mainExpr) && i == 0)
            //        {
            //            colorStart = true;
            //            mainExpr += "and (ProdColor.id=" + catalogColor.id + " ";
            //        }
            //        else
            //            mainExpr += "or ProdColor.id=" + catalogColor.id + " ";
            //    }
            //    if (colorStart)
            //        mainExpr += ") ";
            //}

            //if (AvailFabrics != null)
            //{
            //    bool fabricStart = false;
            //    var lstCatalogFabrics = AvailFabrics.SelectedItems;
            //    for (int i = 0; i < lstCatalogFabrics.Count; i++)
            //    {
            //        FabricsInfo fab = lstCatalogFabrics[i] as FabricsInfo;
            //        if (string.IsNullOrEmpty(mainExpr))
            //            mainExpr += "fabric.id=" + fab.id + " ";
            //        else if (!string.IsNullOrEmpty(mainExpr) && i == 0)
            //        {
            //            fabricStart = true;
            //            mainExpr += "and (fabric.id=" + fab.id + " ";
            //        }
            //        else
            //            mainExpr += "or fabric.id=" + fab.id + " ";
            //    }
            //    if (fabricStart)
            //        mainExpr += ") ";
            //}

            //if (CboPrice.SelectedIndex > 0)
            //{
            //    if (!string.IsNullOrEmpty(mainExpr))
            //        mainExpr += "and ";

            //    if (CboPrice.SelectedIndex == 1)
            //        pricequery += "TemplatePrice<=2000";
            //    else if (CboPrice.SelectedIndex == 2)
            //        pricequery += "TemplatePrice>2000 and TemplatePrice<=3000";
            //    else if (CboPrice.SelectedIndex == 3)
            //        pricequery += "TemplatePrice>3000 and TemplatePrice<=4000";
            //    else if (CboPrice.SelectedIndex == 4)
            //        pricequery += "TemplatePrice>4000 and TemplatePrice<=5000";
            //    else if (CboPrice.SelectedIndex == 5)
            //        pricequery += "TemplatePrice>5000 and TemplatePrice<=6000";
            //    else if (CboPrice.SelectedIndex == 6)
            //        pricequery += "TemplatePrice>6000 and TemplatePrice<=7000";
            //    mainExpr += pricequery;
            //}


            //if (!string.IsNullOrEmpty(mainExpr))
            //{
            //    List<productTemplateBeans> temps = lstMainProdTemplateBeans.AsQueryable().Where(mainExpr).ToList();
            //    //ImageContext.ItemsSource = null;
            //    //ImageContext.ItemsSource = temps;
            //    loadNoStockImg();

            //}
            //else
            //{
            //    //ImageContext.ItemsSource = lstMainProdTemplateBeans;
            //    loadNoStockImg();
            //}

            //if (PhotoCollection != null && AvailFabrics != null && !string.IsNullOrEmpty(pricequery))
            //{
            //    PhotoCollection.ItemsSource = lstMainProdTemplateBeans.AsQueryable().Where(pricequery).ToList().GroupBy(c => c.ProdColor).Select(c => c.Key).ToList();
            //    AvailFabrics.ItemsSource = lstMainProdTemplateBeans.AsQueryable().Where(pricequery).ToList().GroupBy(c => c.fabric).Select(c => c.Key).ToList();
            //}
            //else if (PhotoCollection != null && AvailFabrics != null)
            //{
            //    PhotoCollection.ItemsSource = lstMainProdTemplateBeans.AsQueryable().ToList().GroupBy(c => c.ProdColor).Select(c => c.Key).ToList();
            //    AvailFabrics.ItemsSource = lstMainProdTemplateBeans.AsQueryable().ToList().GroupBy(c => c.fabric).Select(c => c.Key).ToList();
            //}
        }

        public void valueChange(object sender, EventArgs e)
        {
            //lowerLabel.Text = Math.Round(slider.LowerValue).ToString();
            //upperLabel.Text = Math.Round(slider.UpperValue).ToString();
        }

        TouchPoint touchScrollStartPoint;
        TouchPoint touchScrollEndPoint;

        private void ScrollViewer_ScrollChanged_1(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReported;
            Touch.FrameReported += Touch_FrameReported;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.Y != touchScrollEndPoint.Position.Y)
            {
                isScrollMoved = true;
            }
        }

        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void ScrollViewerImageContext_PreviewTouchDown_1(object sender, TouchEventArgs e)
        {
            btnTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void ScrollViewerImageContext_PreviewTouchMove_1(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void ScrollViewerImageContext_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            //if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.Y == touchScrollEndPoint.Position.Y)
            if (touchScrollStartPoint != null && touchScrollEndPoint != null)
            {
                double diffValue = Math.Abs(touchScrollStartPoint.Position.Y - touchScrollEndPoint.Position.Y);
                if (diffValue <= 10)
                {
                    isScrollMoved = false;
                    //r
                }
            }
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            lstProdTemplateBeans = new List<productTemplateBeans>();
            LoadData();
            CboPrice.SelectedIndex = 0;

        }

        public event EventHandler myEventOpenItem2;
        public event EventHandler myEventOpenItem3;

        private void item3_TouchDown_1(object sender, TouchEventArgs e)
        {
            myEventOpenItem3(this, null);
        }

        private void item2_TouchDown_1(object sender, TouchEventArgs e)
        {
            myEventOpenItem2(this, null);
        }
    
    }
}
