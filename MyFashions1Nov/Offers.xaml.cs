﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Nakshatra
{
    /// <summary>
    /// Interaction logic for Offers.xaml
    /// </summary>
    public partial class Offers : UserControl
    {
        public Offers()
        {
            InitializeComponent();
            double ScreenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double ScreenHeight = System.Windows.SystemParameters.PrimaryScreenHeight - 80;

            OfferMainLayout.Height = ScreenHeight;
            OfferMainLayout.Width = ScreenWidth;
        }

        private void ScrollViewer_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        public event EventHandler myEventLoadListing;

        private void Button_TouchDown_1(object sender, TouchEventArgs e)
        {
            myEventLoadListing(this, null);
        }
        
    }
}
